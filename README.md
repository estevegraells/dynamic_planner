# README #

## Cuál es el propósito de este repositorio? ##

* Este es el repositorio con el código de un Planificador APEX para **Procesos Asíncronos** para la plataforma Salesforce, tal y como se describe en la 
[Entrada del blog con la explicación completa](https://forcegraells.com/2018/03/03/planificador-dinamico-procesos-asincronos/).

    ## Contacto ##

* Para cualquier duda, sugerencia o comentario puedes contactarme en mi [Email Personal](esteve.graells@gmail.com).

Un saludo.
