/**
 * Dynamic Planner allowing job conditions
 * <p>
 * <P>Read how to set up Custom Object describing conditions
 *
 * Refer to : https://forcegraells.com for more info

* @author Esteve Graells
* @date March 2018
*
* @description Main class for Dynamic Planner - dinamically plan aync processes on the Salesforce Platform
* allowing processes conditions on process launch
*
* To execute anonymous:
  for (egraells__Process__c p :
  [ SELECT Id, egraells__LastExecutionDate__c, egraells__timeWindowRefresh__c FROM egraells__Process__c]){
    p.egraells__LastExecutionDate__c = null;
    p.egraells__Status__c = 'NO_Ejecutado';
    p.egraells__timeWindowRefresh__c = 1;
    update (p);
  }
  delete [select id from process_log__c limit 10000];
  new DynamicPLanner();

*/


public with sharing class DynamicPlanner implements Schedulable {

    private static final Integer MAX_CONCURRENT_ACTIVE_JOBS_SALESFORCE = 5;

    //Up to 4 queued or active batch jobs are allowed for Apex
    private static final Integer MAX_CONCURRENT_BATCH_JOBS_RUNNING = MAX_CONCURRENT_ACTIVE_JOBS_SALESFORCE - 1;

    public static String Dynamic_Planner_Status = 'Not_Running';


    /*******************************************************************************************************
    * @description Constructor
    */
    public DynamicPlanner() {

        System.debug('Let the Plan be with you...');

        Dynamic_Planner_Status = 'Running';

        execute(null);

        Dynamic_Planner_Status = 'Not_Running';
    }


    /*******************************************************************************************************
    * @description Method to implement the Schedulable Interface. This is the main algorith:
    * alarmed recurrent processes are managed first,
    * followed by the non-alarmed recurrent and
    * finally the single ones (non-recurrent)
    * @param context Schedulable context
    */
    public void execute(SchedulableContext context) {

        Logger.insertEventLog('********* DYNAMIC PLANNER: STARTED *********' + Datetime.now() );

        scheduleRecurrentAlarmedProcesses();
        scheduleNonAlarmedRecurrentProcesses();
        scheduleSingleProcesses();

        Logger.insertEventLog( '********* DYNAMIC PLANNER: ENDED *********' + Datetime.now() );
    }

    /*******************************************************************************************************
    * @description Schedule recurrent alarmed processes
    * @return void
    *
    */
    private void scheduleRecurrentAlarmedProcesses() {

        for (List<Process__c> recurrentAlarmedProcesses :
        [SELECT Id, Name, LastExecutionDate__c
         FROM Process__c
         WHERE schedule_type__c = 'Recurrent'
                AND Status__c  = 'Alarmed'
                AND Status__c != 'Planned']) {

            Logger.insertEventLog(recurrentAlarmedProcesses.size() + ' Recurrent Alarmed Jobs. Will be the first to be planned.');

            for (Process__c p : recurrentAlarmedProcesses) {

                //Is it possible to execute another job? Let's see
                Integer ActiveBatchProcesses = getTotalActiveBatchProcessesOrg();

                if (ActiveBatchProcesses < MAX_CONCURRENT_BATCH_JOBS_RUNNING) {

                    runJob(p.Name);
                    Logger.insertEventLog('RUN ' + p.Name + ' . It was alarmed.');

                    p.Status__c = 'Planned';
                    UPDATE p;

                } else {
                    //There is not slots available for another active process - Alarm the process
                    Logger.insertEventLog(p.Name + ' Alarmed again. Active Jobs: ' + ActiveBatchProcesses);
                }
            }
        }
    }


    /*******************************************************************************************************
    * @description Schedule recurrent non alarmed processes
    *
    * @return void
    */
    private void scheduleNonAlarmedRecurrentProcesses() {

        //For all non alarmed recurrent processes not executed in the last 5'

        List<Process__c> recurrentNonAlarmedProcesses =
        [SELECT Id, Name, LastExecutionDate__c, timeWindowRefresh__c
         FROM Process__c
         WHERE schedule_type__c = 'Recurrent'
                AND Status__c != 'Alarmed'
                AND Status__c != 'Planned' ];

        Logger.insertEventLog( recurrentNonAlarmedProcesses.size() + ' recurrent processes not alarmed require planning');

        Integer ActiveBatchProcesses = getTotalActiveBatchProcessesOrg();

        for (Process__c p : recurrentNonAlarmedProcesses) {

            if ( outsideRefreshWindow(p) ) {
                //Is it possible to execute another Job? Let's see
                if (ActiveBatchProcesses < MAX_CONCURRENT_BATCH_JOBS_RUNNING) {

                    Logger.insertEventLog('RUN ' + p.Name + ' . Active Jobs: ' + ActiveBatchProcesses);
                    runJob(p.Name);

                } else {
                    //There is not slots available for another active process - Alarm the process
                    p.Status__c = 'Alarmed';
                    UPDATE p;

                    Logger.insertEventLog('ALARMED' + p.Name + ' - should be Executed, but max Actived Jobs: ' + ActiveBatchProcesses);
                }

                //ActiveBatchProcesses = getTotalActiveBatchProcessesOrg(); //This makes sense only if you have very short processing jobs but many of them

            } else {
                Logger.insertEventLog(p.name + ' does not need to run, still inside refresh windows');
            }
        }
    }


    /*******************************************************************************************************
    * @description Manage single processes (non-recurrent)
    * @return void
    *
    */
    private void scheduleSingleProcesses() {

        // We schedule single processes, those are processes that run once a day
        // Do not schedule processes that were refreshed inside Refresh Window

        for ( List<Process__c> singleProcessesNotExecuted :
        [SELECT Id, Name, Not_before__c, timeWindowRefresh__c, FathersList__c, LastExecutionDate__c
         FROM Process__c
         WHERE  schedule_type__c = 'Single'
                AND  Status__c != 'Planned'
                ORDER BY Priority__c ASC]){

            Integer ActiveBatchProcesses = getTotalActiveBatchProcessesOrg();

            Logger.insertEventLog( singleProcessesNotExecuted.size() + ' Single Left Jobs to plan. Active Jobs: ' + ActiveBatchProcesses);

            //List for processes that will not be planned because no slots are available
            List<String> processesNotPlannedNotSlotavailable = new List<String>();

            for (Process__c p : singleProcessesNotExecuted) {

                //Is it possible to execute another batch? We always keep 1 for Recurrent Processes
                if ( ActiveBatchProcesses < MAX_CONCURRENT_BATCH_JOBS_RUNNING ) {

                    if (areProcessDependenciesOK (p)) {

                        runJob(p.Name);
                        Logger.insertEventLog('RUN ' + p.Name + ' . Dependencies OK. Active Jobs: ' + ActiveBatchProcesses);

                    }else Logger.insertEventLog('DO NOT RUN ' + p.Name + ' . Some dependencies are not met');

                } else processesNotPlannedNotSlotavailable.add(p.name);

                //ActiveBatchProcesses = getTotalActiveBatchProcessesOrg(); //This makes sense only if you have very short processing jobs but many of them
            }

            String listString = '';

            for (String processName : processesNotPlannedNotSlotavailable){ listString = listString + ';' + processName;}

            Logger.insertEventLog( 'NO SLOTS AVAILABLE FOR: ' + listString + '. Max Actived Jobs: ' + ActiveBatchProcesses);
        }
    }

    /*******************************************************************************************************
    * @description face-method to check dependencies on a process at the current time
    * @param process class to be checked
    * @param fieldName the name of the field to look up
    * @return dependencies are OK or False
    *
    */
    private boolean areProcessDependenciesOK(Process__c p) {

        boolean outRefreshWindow = outsideRefreshWindow(p); // Is the process outside its refresh window?
        boolean doNotStart = doNotStartBefore(p);  // Does the process have starting time condition?
        boolean allAncestorsExecuted = myAncestorsAllExecuted(p);   // Where all ancestors executed previously?

        Logger.insertEventLog(p.Name + ' Checking Dependencies -- Refresh Window: ' + outRefreshWindow
                                     + ' -- Not Start Before time: '                + doNotStart
                                     + ' -- All Ancestors executed: '               + allAncestorsExecuted);

        return outRefreshWindow & doNotStart & allAncestorsExecuted;
    }

    /*******************************************************************************************************
    * @description Checks if the last execution is older enough to run it again
    * @param process class to be checked
    * @return true or false it's older enough to run again the process
    *
    */
    private boolean outsideRefreshWindow(Process__c p) {

        if ( (p.LastExecutionDate__c == null) || (p.timeWindowRefresh__c == null) ) {
            return true;
        } else {
            Integer minutesSinceLastRefresh = getMinutesBetweenDates (datetime.now() , p.LastExecutionDate__c);
            return ( minutesSinceLastRefresh > p.timeWindowRefresh__c);
        }
    }

    /*******************************************************************************************************
    * @description Checks if there is a condition not to start at a exac time
    * @param process class to be checked
    * @return true or false if there is a condition and right now it's passesd
    */
    private boolean doNotStartBefore(Process__c p) {

        if (p.Not_before__c != null) {

            Logger.insertEventLog(p.Name + ' Do not Run Before: ' + p.Not_before__c + ' + Now is: ' + system.now().time() );

            return ( system.now().time() > p.Not_before__c );

        } else return true;
    }


    /*******************************************************************************************************
    * @description check if there is a condition saying other processes (called ancestors) must be executed before this one
    * @param process class to be checked
    * @return true or false if there are ancesthors, and in that case, those ancestors have been executed before this process
    */
    private boolean myAncestorsAllExecuted(Process__c p) {

        List<String> ParentsStringList = null;

        //If the process have ancestors, lets check if they were all executed previuosly
        if ( !String.isEmpty(p.FathersList__c) ){

            ParentsStringList = p.FathersList__c.split(';');//convert the father string list to Apex list

            for (List<Process__c> fathersProcesses :
            [SELECT Id, Name, LastExecutionDate__c, timeWindowRefresh__c
            FROM Process__c
            WHERE Name in :ParentsStringList])
            {
                for (Process__c father : fathersProcesses) {

                    //Logger.insertEventLog( p.Name + ' - My Ancestor ' + father.Name + ' was last executed: ' + father.LastExecutionDate__c);

                    if (father.LastExecutionDate__c != null) {

                        //if just one father is outdated, the process gets a FALSE
                        Decimal minutesSinceLastRefresh = getMinutesBetweenDates(datetime.now(), father.LastExecutionDate__c);

                        //if the father last execution older that his refresh window, then return false, father needs refresh
                        if (minutesSinceLastRefresh > father.timeWindowRefresh__c) return false;

                    } else return false; //If father never executed, then stop and return FALSE
                }
            }
        }

        return true;
    }


    /*
    * Get active Batch processes: Queued, Processing, Aborted, Completed, Failed, Preparing, Holding
    * */
    /*******************************************************************************************************
    * @description Get total active process in the system
    * @return number of processes on Processing, Preparing or Holding state
    */
    private Integer getTotalActiveBatchProcessesOrg() {

        List<AsyncApexJob> runingBatchProcesses =
        [SELECT Id, MethodName
        FROM AsyncApexJob
        WHERE Status = 'Processing' OR Status = 'Preparing' OR Status = 'Holding' /*OR Status = 'Queued'*/];

        return runingBatchProcesses.size();
    }



    /*******************************************************************************************************
    * @description creates a process based on his name - Run Forrest Run!!
    * @param process_name the name of the process to run
    */
    private void runJob(String jobName) {

        ID newJobID;

        //Dynamically create a Job with the name received
        Type t = Type.forName(jobName);

        //As I do not know which interface implements the Job (Queueable or Batchable),
        // and there is no option to check if the class implements an interface in Apex
        // (or at least I couldn't find it) lets do it by try-error
        try {
            Database.Batchable< sObject > newJob = ((Database.Batchable< sObject >) t.newInstance());
            newJobID = Database.executeBatch(newJob);
        } catch (Exception e) {
            Queueable newJob = (Queueable) t.newInstance();
            newJobID = System.enqueueJob(newJob);
            //Logger.insertEventLog('Queueable found or Platform Error: ' + e);
        }

        //Get info and check creation was successful
        List<AsyncApexJob> newProcess = [SELECT Id, Status FROM AsyncApexJob WHERE Id = :newJobId  LIMIT 1];
        Logger.insertEventLog( 'CREATED ' + jobName + ' Id: ' + newJobId + ' - State: ' + newProcess[0].Status);
    }

    /**
    * Get hours are between 2 dates
    * */
    /*******************************************************************************************************
    * @description Get minutes between 2 dates
    * @param datetime1 future time
    * @param datetime2 past time
    * @return minutes between these dates
    */
    private Integer getMinutesBetweenDates(Datetime StartTime, Datetime EndTime){

        Decimal milis = StartTime.getTime() - EndTime.getTime();

        return Math.round(milis/1000.0/60.0);

    }
}

