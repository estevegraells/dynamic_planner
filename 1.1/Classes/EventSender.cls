/**
 * Created by estev on 01/02/2018.
 */

public with sharing class EventSender {


    public static void sendEventBus(String p, String Description){

        //Sent Event a process has completed
        ProcessCompleted__e processFinishedEvent = new ProcessCompleted__e(Process_Name__c = p, Description__c = Description);

        Database.SaveResult result = EventBus.publish(processFinishedEvent);

        if ( !result.isSuccess() ) {
            for (Database.Error err : result.getErrors()) {
                System.debug('Error returned: ' + err.getStatusCode() + ' - ' + err.getMessage());
            }
        }
    }
}