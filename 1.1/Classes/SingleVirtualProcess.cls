/**
 * Created by estev on 02/02/2018.
 */

global with sharing virtual class SingleVirtualProcess implements Database.Batchable<sObject> {

    protected String processName = '';

    public SingleVirtualProcess() {

    }

    global Database.QueryLocator start(Database.BatchableContext BC) {

        Process__c p = [ SELECT Id, Status__c FROM Process__c WHERE Name = :processName LIMIT 1];
        p.Status__c = 'Planned';
        update(p);

        return Database.getQueryLocator('SELECT Id from account LIMIT 1');
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {

        //Update Process information
        Process__c p = [ SELECT Id, Name, LastExecutionDate__c, Simulate_Time__c FROM Process__c WHERE Name = :processName LIMIT 1];

        //Logger.insertEventLog(processName + ' must run for: ' + p.Simulate_Time__c + ' seconds');

        Long startTimestamp =  System.currentTimeMillis();

        businessLogic(p.Simulate_Time__c * 1000);

        Logger.insertEventLog(p.Name + ' has ran for: ' + ( System.currentTimeMillis() - startTimestamp)/1000 + ' seconds');

    }

    global void finish(Database.BatchableContext BC) {

        // Query the AsyncApexJob object to retrieve the current job's information.
        AsyncApexJob a = [SELECT Id, Status FROM AsyncApexJob WHERE Id = :BC.getJobId()];

        //Update Process information
        Process__c p = [ SELECT Id, Name, LastExecutionDate__c, Status__c FROM Process__c WHERE Name = :processName];
        p.LastExecutionDate__c = DateTime.now();
        p.Status__c = 'Executed';

        update(p);

        Logger.insertEventLog('EXECUTED ' + p.Name);

        //Send Event notificacion to announce its been finished
        EventSender.sendEventBus(processName, processName + ' Executed');
    }

    /*
   * Good Dreams Sweet heart
   * */
    global void businessLogic(decimal milliseconds) {

        Long timeDiff = 0;
        DateTime firstTime = System.now();
        do {
            timeDiff = System.now().getTime() - firstTime.getTime();
        } while (timeDiff <= milliseconds);
    }

}