/**
 * Created by estev on 26/02/2018.
 */

public with sharing class P21Process implements  Queueable{

    public void P21Process(){

        Process__c p = [ SELECT Id, Name, Status__c, LastExecutionDate__c, Simulate_Time__c FROM Process__c WHERE Name = 'P21Process' LIMIT 1];
        p.Status__c = 'Planned';
        update(p);

    }

    public void execute(QueueableContext  context) {

        Process__c p = [ SELECT Id, Name, Status__c, LastExecutionDate__c, Simulate_Time__c FROM Process__c WHERE Name = 'P21Process' LIMIT 1];

        businessLogic(p.Simulate_Time__c);

        p.LastExecutionDate__c = DateTime.now();
        p.Status__c = 'Executed';
        update(p);

        //Send Event notification to announce its been finished
        EventSender.sendEventBus('P21Process', 'P21Process Completado.');

    }

    private void businessLogic(decimal milliseconds) {

        Long startTimestamp =  System.currentTimeMillis();
        Long timeDiff = 0;
        DateTime firstTime = System.now();
        do {
            timeDiff = System.now().getTime() - firstTime.getTime();
        } while (timeDiff <= milliseconds*1000);

        Logger.insertEventLog('P21Process has ran for: ' + (System.currentTimeMillis() - startTimestamp)/1000 + ' seconds');
        Logger.insertEventLog('EXECUTED P21Process');

    }


}