/**
 * Created by estev on 02/02/2018.
 */

global with sharing virtual class RecurrentVirtualProcess implements Database.Batchable<sObject> {


    private static boolean NOTIFY_ON_FINISH_RECURRENT = false;

    public String processName = '';

    public  RecurrentVirtualProcess(){

    }


    global Database.QueryLocator start(Database.BatchableContext BC){

        Process__c p = [ SELECT Id, Name, Status__c FROM Process__c WHERE Name = :processName LIMIT 1];
        p.Status__c = 'Planned';
        update (p);

        return Database.getQueryLocator('SELECT Id from account LIMIT 1');
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope){

        Long startTimestamp = System.currentTimeMillis();

        Process__c p = [ SELECT Id, Name, LastExecutionDate__c, Simulate_Time__c FROM Process__c WHERE Name = :processName LIMIT 1];
        businessLogic(p.Simulate_Time__c * 1000);

    }

    global void finish(Database.BatchableContext BC) {
        // Get the ID of the AsyncApexJob representing this batch job from Database.BatchableContext.
        // Query the AsyncApexJob object to retrieve the current job's information.
        AsyncApexJob a = [SELECT Id, Status FROM AsyncApexJob WHERE Id = :BC.getJobId()];

        //Update Process information
        Process__c p = [ SELECT Id, Name, Status__c, LastExecutionDate__c FROM Process__c WHERE Name = :processName];
        p.LastExecutionDate__c = DateTime.now();
        p.Status__c = 'Executed';
        update(p);

        Logger.insertEventLog('EXECUTED ' + p.Name);

        //Send Event notificacion to announce its been finished
        if (NOTIFY_ON_FINISH_RECURRENT)
            EventSender.sendEventBus(processName, processName + ' Executed');
    }

    /*
    * Good Dreams Sweet heart
    * */

    global void businessLogic(decimal milliseconds) {

        Long timeDiff = 0;
        DateTime firstTime = System.now();
        do {
            timeDiff = System.now().getTime() - firstTime.getTime();
        } while (timeDiff <= milliseconds);
    }
}