/**
 * Dynamic Planner Trigger to receive Job Executed Event
 *
 * Refer to : https://forcegraells.com/apex-dynamic-job-planner for more info
 * *
 * @author Esteve Graells
 * @version 1.0
 */

trigger processCompletedTrigger on egraells__ProcessCompleted__e (after insert) {


    // Iterate through each notification.
    for (egraells__ProcessCompleted__e event : Trigger.New) {
        Logger.insertEventLog(event.Process_Name__c
                + ' sent event - EXECUTED. Dynamic Planner to be executed - Probably 1 slot free.');
    }

    if (DynamicPlanner.Dynamic_Planner_Status == 'Not_Running'){
        new DynamicPlanner();
    } else Logger.insertEventLog('Dyanmic Planner already Running');

}