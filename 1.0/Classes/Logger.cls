/**
 * Created by estev on 31/01/2018.
 */

/*******************************************************************************************************
* @description Insert an Event in the Log Object
*
* */
public class Logger {


    private static Integer i = 0;

    public static void insertEventLog(String Description) {

        i++;

        Process_Log__c newLog = new Process_Log__c(Description__c = i + '.' + Description);

        INSERT newLog;

    }

}